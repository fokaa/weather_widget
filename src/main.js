import Vue from 'vue';
import App from './App.vue';
import store from './store';
import 'document-register-element/build/document-register-element'
import vueCustomElement from 'vue-custom-element'
import VueSocketIO from 'vue-socket.io';
import SocketIO from 'socket.io-client';

const options = {
    path: '/ws',
    transports: ['websocket', 'polling']
};
const server = 'deleted';

Vue.config.productionTip = false

Vue.use(vueCustomElement)

Vue.use(new VueSocketIO({
  debug: true,
  connection: SocketIO(server, options), 
  vuex: {
    store,
    actionPrefix: "SOCKET_",
  }
}));

App.store = store
Vue.customElement('allmeteo-widget', App)

