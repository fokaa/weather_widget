import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function calcPress(pressure, temperature, elevation, height){
    var result = pressure;
    if (pressure && temperature){
        let dZ = 0;
        if (elevation) {
            dZ = elevation
        }
        if (height){
            dZ += height;
        }
        result = pressure * Math.pow ( (1 - (0.0065 * dZ) / (temperature + (0.0065 * dZ))), -5.257);
    }
    return result;
}

export default new Vuex.Store({
    state: {
        time: null,
        name: '',
        sn: 0,
        position: null,
        weather: '',
        data: {},
        action: 'waiting',
        station: '',
        error: '',
        settings: {
            'temp': 'c',
            'press': 'hpa'
        }
    },
    mutations: {
        setaction(state, action) {
            state.action = action.text;
        },
        setdata(state, action) {
            state.data = action.data;
        },
        setname(state, action) {
            state.name = action.text;
        },
        settime(state, action) {
            state.time = action.text;
        },
        setweather(state, action) {
            state.weather = action.text;
        },
        setposition(state, action) {
            state.position = action.text;
        },
        setstation(state, action) {
            state.station = action.text;
            if (this._vm.$socket) {
                this._vm.$socket.emit('station', state.station)
            }
        }
    },
    actions: {
        /* eslint-disable no-alert, no-debugger */
        "SOCKET_data"(context, data) {
            data = JSON.parse(data)
            if (data.data.pressure && data.data.temperature && data.elevation) {
                var pressure = calcPress(data.data.pressure, data.data.temperature,
                    data.elevation, data.height ? data.height : 0)
                if (pressure)
                    data.data.pressure = pressure
            }
                
            context.commit('setaction', { text: 'data' });
            context.commit('setname', { text: data.name });
            context.commit('settime', { text: data.time });
            context.commit('setweather', { text: data.weather });
            context.commit('setposition', { text: data.position });
            context.commit('setdata', { data: data.data});
        },
        "SOCKET_connect"(context) {
            context.commit('setaction', { text: 'connected' })
        },
        "SOCKET_disconnect"(context) {
            context.commit('setaction', { text: 'disconnected' })
            if (context.state.station) {
                context.commit('setstation', { text: context.state.station })
            }
        },
        changeStation(context, station) {
            if (station) {
                context.commit('setstation', { text: station.text })
            }
        }
        /* eslint-disable no-alert, no-debugger */
    }
})